<?php

namespace Dweipert\DevOps\UnattendedUpgrades;

use PHPIAC\Modules\AptModule;
use PHPIAC\Modules\TemplateModule;
use PHPIAC\Role\RoleInterface;
use PHPIAC\Task;

class UnattendedUpgrades implements RoleInterface
{
    public function __invoke(array $config = []): array
    {
        $config = array_replace_recursive(
            include dirname(__DIR__) . '/files/config.php',
            $config
        );

        return [
            (new Task())->setModule(new AptModule([
                'package' => 'unattended-upgrades',
                'updateCache' => true,
            ])),
            (new Task())->setModule(new TemplateModule([
                'src' => dirname(__DIR__) . '/files/auto-upgrades.twig',
                'dest' => '/etc/apt/apt.conf.d/20auto-upgrades',
                'vars' => $config,
                'owner' => 'root',
                'group' => 'root',
                'mode' => 0644,
                'force' => true,
            ])),
            (new Task())->setModule(new TemplateModule([
                'src' => dirname(__DIR__) . '/files/unattended-upgrades.twig',
                'dest' => '/etc/apt/apt.conf.d/50unattended-upgrades',
                'vars' => $config,
                'owner' => 'root',
                'group' => 'root',
                'mode' => 0644,
                'force' => true,
            ])),
        ];
    }
}
