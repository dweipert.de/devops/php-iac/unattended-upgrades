<?php

require dirname(__DIR__) . '/vendor/autoload.php';

return [
  'host' => 'vagrant',
  'user' => 'vagrant',
  'private_key_file' => __DIR__ . '/.vagrant/machines/default/virtualbox/private_key',

  'tasks' => [
    ...(new \Dweipert\DevOps\UnattendedUpgrades\UnattendedUpgrades())([
        'unattended_mail' => 'test@example.org',
    ]),
  ],
];
